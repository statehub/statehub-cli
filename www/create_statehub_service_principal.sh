#!/bin/bash
set -e

generate_credentials_json() {
    cat <<EOF
{
  "subscriptionId": "$SELECTED_SUBSCRIPTION_ID",
  "tenantId": "$(echo "$SELECTED_SUBSCRIPTION" | jq -r '.tenantId')",
  "clientId": "$APP_ID",
  "clientSecret": "$APP_SECRET"
}
EOF
}

### Select a subscription

SUBSCRIPTIONS=$(az account list)

echo 'Available subscriptions: '
echo "$SUBSCRIPTIONS" | jq -r '(["ID","NAME", "IS_DEFAULT"] | (., map(length*"-"))), (.[] | [.id, .name, .isDefault]) | @tsv' | column -ts $'\t'
echo

while true; do
    echo -n 'Enter subscription id: '
    read -r SELECTED_SUBSCRIPTION_ID

    SELECTED_SUBSCRIPTION=$(echo "$SUBSCRIPTIONS" | jq -r --arg id "$SELECTED_SUBSCRIPTION_ID" '.[]|select(.id==$id)')

    if [[ $SELECTED_SUBSCRIPTION ]]; then
        break
    fi
done

echo "Press enter to continue.."
read -r REPLY

echo 'Registering app --- '
echo '. Creating app registration'
APP_ID=$(az ad app create --display-name 'Statehub' --only-show-errors | jq -r '.appId')
APP_SECRET=$(az ad app credential reset --id "$APP_ID" --years 1000 --only-show-errors | jq -r '.password')

APP_SP=$(az ad sp list --query "[?appId=='$APP_ID']" --all | jq -r 'first(.[])')
if [[ $APP_SP == "" ]]; then
    echo '. Creating service principal'
    az ad sp create --id "$APP_ID" -o none
else
    echo ". Using existing service principal"
fi

# assign roles to the apps
echo 'Assigning roles to the app'
sleep 8s # a new service principal is not usable for role assignment instantly, neither after a couple of seconds.. :)

echo 'Assigning Contributor role to Statehub shared images app'
az role assignment create --assignee "$APP_ID" --role 'Contributor' -o none

### Print results

echo "--------------------------------------------------------------------------------"
echo "Save and use the following JSON to onboard credentials into Statehub"

AZURE_TENANT_ID=$(echo "$SELECTED_SUBSCRIPTION" | jq -r '.tenantId')
AZURE_CLIENT_ID=$APP_ID
AZURE_CLIENT_SECRET=$APP_SECRET

echo "AZURE_TENANT_ID:" $AZURE_TENANT_ID
echo "AZURE_CLIENT_ID:" $AZURE_CLIENT_ID
echo "AZURE_CLIENT_SECRET:" $AZURE_CLIENT_SECRET

# CREDS_JSON=$(generate_credentials_json)
# echo $(echo $CREDS_JSON | base64 -w 0)
