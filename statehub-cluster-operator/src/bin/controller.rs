//
// Copyright (c) 2022 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

#![cfg_attr(feature = "pedantic", warn(clippy::pedantic))]
#![warn(clippy::use_self)]
#![warn(clippy::map_flatten)]
#![warn(clippy::map_unwrap_or)]
#![warn(deprecated_in_future)]
#![warn(future_incompatible)]
#![warn(noop_method_call)]
#![warn(unreachable_pub)]
#![warn(missing_debug_implementations)]
#![warn(rust_2018_compatibility)]
#![warn(rust_2021_compatibility)]
#![warn(rust_2018_idioms)]
#![warn(unused)]
#![deny(warnings)]

use std::sync::Arc;

use futures::StreamExt;
use kube::api;
use kube::runtime::controller::{Context, Controller, ReconcilerAction};
use statehub_cluster_operator::cluster::Operator;
use statehub_cluster_operator::StatehubCluster;
use tokio::time;

struct ControllerState {
    operator: Operator,
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let config = kube::Config::infer().await?;
    let operator = Operator::with_config(config)?;
    let clusters = operator.clusters_api();
    let controller = ControllerState { operator };
    let context = Context::new(controller);

    let lp = api::ListParams::default();
    Controller::new(clusters, lp)
        // .reconcile_all_on(reload_rx.map(|_| ()))
        .shutdown_on_signal()
        .run(reconcile, error_policy, context)
        .for_each(|res| async move {
            match res {
                Ok(o) => log::info!("reconciled {:?}", o),
                Err(e) => log::warn!("reconcile failed: {}", e),
            }
        })
        .await;

    Ok(())
}

async fn reconcile(
    cluster: Arc<StatehubCluster>,
    ctx: Context<ControllerState>,
) -> kube::Result<ReconcilerAction> {
    // log::info!("Reconciling cluster {:?}", statehub);

    log::info!("working hard");
    time::sleep(time::Duration::from_secs(2)).await;
    log::info!("hard work is done!");

    let controller = ctx.get_ref();
    let namespace = controller.operator.create_namespace(&cluster).await?;

    log::info!("Namespace created: {:?}", namespace);

    Ok(ReconcilerAction {
        requeue_after: Some(time::Duration::from_secs(300)),
    })
}

fn error_policy(_error: &kube::Error, _ctx: Context<ControllerState>) -> ReconcilerAction {
    ReconcilerAction {
        requeue_after: Some(time::Duration::from_secs(1)),
    }
}
