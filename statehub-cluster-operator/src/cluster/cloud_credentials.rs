//
// Copyright (c) 2022 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use serde_json as json;

use super::*;

const STATEHUB_CLOUD_CREDS_SECRET_TYPE: &str = "statehub.io/cloud-credential";
const STATEHUB_AZURE_CREDS_SECRET_NAME: &str = "statehub-azure-creds";
const STATEHUB_AWS_CREDS_SECRET_NAME: &str = "statehub-aws-creds";

impl Operator {
    pub async fn create_aws_credentials_secret(
        &self,
        cluster: &StatehubCluster,
        aws: json::Value,
    ) -> kube::Result<corev1::Secret> {
        self.create_cloud_credentials_secret(
            STATEHUB_AWS_CREDS_SECRET_NAME,
            cluster.namespace(),
            aws,
        )
        .await
    }

    pub async fn create_azure_credentials_secret(
        &self,
        cluster: &StatehubCluster,
        azure: json::Value,
    ) -> kube::Result<corev1::Secret> {
        self.create_cloud_credentials_secret(
            STATEHUB_AZURE_CREDS_SECRET_NAME,
            cluster.namespace(),
            azure,
        )
        .await
    }

    async fn create_cloud_credentials_secret(
        &self,
        name: &str,
        namespace: &str,
        creds: json::Value,
    ) -> kube::Result<corev1::Secret> {
        let creds = creds
            .as_object()
            .into_iter()
            .flatten()
            .map(|(key, value)| (key, value.as_str().unwrap_or_default()));
        let secret = corev1::Secret::new(name)
            .r#type(STATEHUB_CLOUD_CREDS_SECRET_TYPE)
            .string_data(creds);
        self.kubectl
            .ensure_namespaced_k_is_installed_forced(secret, namespace)
            .await
    }

    pub async fn check_aws_credentials(
        &self,
        cluster: &StatehubCluster,
    ) -> kube::Result<CheckObject<corev1::Secret>> {
        self.check_namespaced_object(cluster.namespace(), STATEHUB_AWS_CREDS_SECRET_NAME)
            .await
    }

    pub async fn check_azure_credentials(
        &self,
        cluster: &StatehubCluster,
    ) -> kube::Result<CheckObject<corev1::Secret>> {
        self.check_namespaced_object(cluster.namespace(), STATEHUB_AZURE_CREDS_SECRET_NAME)
            .await
    }
}
