//
// Copyright (c) 2022 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use kube::ResourceExt;

use super::*;

impl Operator {
    pub async fn create_configmap(
        &self,
        cluster: &StatehubCluster,
        default_state: &str,
        api: &str,
        cleanup_grace: &str,
    ) -> kube::Result<corev1::ConfigMap> {
        let name = cluster.name();
        let data = [
            ("cluster-name", name.as_str()),
            ("default-state", default_state),
            ("api-url", api),
            ("cleanup-grace", cleanup_grace),
        ];
        let config_map = corev1::ConfigMap::new(STATEHUB_CLUSTER_CONFIGMAP_NAME).data(data);

        self.kubectl
            .ensure_namespaced_k_is_installed_forced(config_map, cluster.namespace())
            .await
    }

    pub async fn delete_configmap(&self, cluster: &StatehubCluster) -> kube::Result<()> {
        let dp = self.kubectl.delete_params();
        let secrets = self.kubectl.secrets(cluster.namespace());
        kube::runtime::wait::delete::delete_and_finalize(
            secrets,
            STATEHUB_CLUSTER_CONFIGMAP_NAME,
            &dp,
        )
        .await
        .map_err(|err| match err {
            kube::runtime::wait::delete::Error::NoUid => todo!(),
            kube::runtime::wait::delete::Error::Delete(err) => err,
            kube::runtime::wait::delete::Error::Await(_) => todo!(),
        })
    }

    pub async fn check_configmap(
        &self,
        cluster: &StatehubCluster,
    ) -> kube::Result<CheckObject<corev1::ConfigMap>> {
        self.check_namespaced_object(cluster.namespace(), STATEHUB_CLUSTER_CONFIGMAP_NAME)
            .await
    }
}
