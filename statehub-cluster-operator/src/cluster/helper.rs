//
// Copyright (c) 2022 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use serde::de;

use super::*;

impl super::Operator {
    #[must_use]
    pub fn clusters_api(&self) -> kube::Api<StatehubCluster> {
        self.kubectl.api::<StatehubCluster>()
    }

    pub async fn get_clusters(&self) -> kube::Result<Vec<StatehubCluster>> {
        let lp = self.kubectl.list_params();
        self.clusters_api().list(&lp).await.map(|list| list.items)
    }

    pub(crate) async fn find_namespaced_resource<K>(
        &self,
        namespace: &str,
        name: &str,
    ) -> kube::Result<Option<K>>
    where
        K: kube::Resource,
        <K as kube::Resource>::DynamicType: Default,
        K: Clone + de::DeserializeOwned + fmt::Debug,
    {
        let lp = self.kubectl.list_params();
        let object = self
            .kubectl
            .namespaced_api::<K>(namespace)
            .list(&lp)
            .await?
            .items
            .into_iter()
            .find(|object| object.name() == name);
        Ok(object)
    }
}
