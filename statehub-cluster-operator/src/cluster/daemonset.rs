//
// Copyright (c) 2022 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use super::*;

const STATEHUB_CSI_NODE_NAME: &str = "statehub-csi-node";

impl Operator {
    pub async fn check_statehub_csi_node(
        &self,
        cluster: &StatehubCluster,
    ) -> kube::Result<CheckObject<appsv1::DaemonSet>> {
        self.check_namespaced_object(cluster.namespace(), STATEHUB_CSI_NODE_NAME)
            .await
    }
}
