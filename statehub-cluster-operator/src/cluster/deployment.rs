//
// Copyright (c) 2022 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use super::*;

const STATE_CONTROLLER_NAME: &str = "state-controller";
const STATEHUB_CONTROLLER_NAME: &str = "statehub-controller";
const STATEHUB_CSI_CONTROLLER_NAME: &str = "statehub-csi-controller";

impl Operator {
    pub async fn check_state_controller(
        &self,
        cluster: &StatehubCluster,
    ) -> kube::Result<CheckObject<appsv1::Deployment>> {
        self.check_namespaced_object(cluster.namespace(), STATE_CONTROLLER_NAME)
            .await
    }

    pub async fn check_statehub_controller(
        &self,
        cluster: &StatehubCluster,
    ) -> kube::Result<CheckObject<appsv1::Deployment>> {
        self.check_namespaced_object(cluster.namespace(), STATEHUB_CONTROLLER_NAME)
            .await
    }

    pub async fn check_statehub_csi_controller(
        &self,
        cluster: &StatehubCluster,
    ) -> kube::Result<CheckObject<appsv1::Deployment>> {
        self.check_namespaced_object(cluster.namespace(), STATEHUB_CSI_CONTROLLER_NAME)
            .await
    }
}
