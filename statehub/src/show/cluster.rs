//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use super::*;

const CLUSTER: Emoji<'static, 'static> = Emoji("☸️", "*");

impl Show for v0::Cluster {
    fn show(&self) -> String {
        format!(
            "{} {:>24} [{:#}]",
            CLUSTER,
            self.name,
            self.locations.show()
        )
    }

    fn detailed_show(&self) -> String {
        let helm = k8s::Helm::skip();
        let namespace = self.namespace.as_deref().unwrap_or("statehub-system");
        let helm = helm.install_command(self, None, namespace);
        format!(
            "{}\n{}\n{}\n{}\n{}\n{}",
            format_args!("Cluster:     {}", self.name),
            format_args!("Id:          {}", self.id),
            format_args!("Locations:   {}", self.locations.show()),
            format_args!("Created:     {}", HumanTime::from(self.created)),
            format_args!("Modified:    {}", HumanTime::from(self.modified)),
            format_args!("Helm install:\n{}", helm.detailed_show())
        )
    }
}

impl Show for Vec<v0::Cluster> {
    fn show(&self) -> String {
        self.iter().map(Show::show).join("\n")
    }
}

impl Show for v0::ClusterLocations {
    fn show(&self) -> String {
        let aws = self
            .aws
            .iter()
            .map(|location| format!("{:#}", location.region));
        let azure = self
            .azure
            .iter()
            .map(|location| format!("{:#}", location.region));
        aws.chain(azure).join(", ")
    }
}
