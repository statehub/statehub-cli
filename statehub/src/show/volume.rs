//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use super::*;

impl Show for v0::LocationVolumeStatus {
    fn show(&self) -> String {
        let msg = self
            .msg
            .as_ref()
            .map(|msg| format!(" {}", msg))
            .unwrap_or_default();
        format!("{}{}", self.value.show(), msg)
    }
}

impl Show for v0::StateLocationVolumeProgress {
    fn show(&self) -> String {
        let progress = 100 * self.bytes_synchronized / self.bytes_total;
        format!("{}%", progress)
    }
}

impl Show for v0::VolumeLocation {
    fn show(&self) -> String {
        format!("{} {}", self.name, self.status.show())
    }

    fn detailed_show(&self) -> String {
        let progress = self
            .progress
            .as_ref()
            .map(|progress| format!("({})", progress.show()))
            .unwrap_or_default();
        format!("{} {}{}", self.name, self.status.detailed_show(), progress)
    }
}

impl Show for v0::Volume {
    fn show(&self) -> String {
        format!(
            "{:<32} {:>8} GiB {:>6} active: {}{}",
            self.name,
            self.size_gi,
            self.fs_type,
            self.active_location.as_deref().unwrap_or("None"),
            self.progress()
                .map(|(status, progress)| format!(" ({} {} done)", status.show(), progress.show()))
                .unwrap_or_default()
        )
    }

    fn detailed_show(&self) -> String {
        let locations = self
            .locations
            .iter()
            .map(|location| format!("  {}", location.detailed_show()))
            .join("\n");
        format!(
            "{}\n{}\n{}\n{}\n{}",
            format_args!("Volume   :{:>60}", self.name),
            format_args!("Size     :{:>56} GiB", self.size_gi),
            format_args!("FS Type  :{:>60}", self.fs_type),
            format_args!(
                "Active   :{:>60}",
                self.active_location.as_deref().unwrap_or("None")
            ),
            format_args!("Locations:\n{}", locations)
        )
    }
}

impl Show for Vec<v0::Volume> {
    fn show(&self) -> String {
        self.iter().map(Show::show).join("\n")
    }

    fn detailed_show(&self) -> String {
        self.iter().map(Show::detailed_show).join("\n")
    }
}
