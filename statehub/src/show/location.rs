//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use super::*;

impl Show for Location {
    fn show(&self) -> String {
        format!("{:#}", self)
    }
}

impl Show for Vec<Location> {
    fn show(&self) -> String {
        self.iter().map(Show::show).join(" and ")
    }
}

impl Show for &[Location] {
    fn show(&self) -> String {
        self.iter().map(Show::show).join(" and ")
    }
}
