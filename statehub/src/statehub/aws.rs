//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use clap::Parser;
use serde::{Deserialize, Serialize};
use serde_with::skip_serializing_none;

#[skip_serializing_none]
#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, Parser)]
#[serde(rename_all = "UPPERCASE")]
pub(crate) struct AwsCredentials {
    // #[clap(help = "AWS Access Key ID", long, requires = "aws_secret_access_key")]
    #[clap(help = "AWS Access Key ID", long)]
    aws_access_key_id: Option<String>,
    // #[clap(help = "AWS Secret Access Key", long, requires = "aws_access_key_id")]
    #[clap(help = "AWS Secret Access Key", long)]
    aws_secret_access_key: Option<String>,
}

impl AwsCredentials {
    pub(super) fn validate(&self) -> anyhow::Result<()> {
        let all = self.aws_access_key_id.is_some() && self.aws_secret_access_key.is_some();

        let nothing = self.aws_access_key_id.is_none() && self.aws_secret_access_key.is_none();

        if all || nothing {
            Ok(())
        } else {
            Err(anyhow::anyhow!(
                "Either all of '{}' and '{}' options must be set or none of them",
                "aws-access-key-id",
                "aws-secret-access-key",
            ))
        }
    }

    pub(super) fn missing(&self) -> bool {
        self.aws_access_key_id.is_none() || self.aws_secret_access_key.is_none()
    }

    pub(super) fn new(access_key_id: String, secret_access_key: String) -> Self {
        let aws_access_key_id = Some(access_key_id);
        let aws_secret_access_key = Some(secret_access_key);
        Self {
            aws_access_key_id,
            aws_secret_access_key,
        }
    }
}

#[cfg(test)]
mod tests {
    use serde_json as json;

    use super::*;

    #[test]
    fn deserialize() {
        let aws = AwsCredentials {
            aws_access_key_id: String::new().into(),
            aws_secret_access_key: String::new().into(),
        };

        let aws = json::to_value(aws).unwrap();
        assert!(aws.is_object());
        assert!(aws.get("AWS_ACCESS_KEY_ID").is_some());
        assert!(aws.get("AWS_SECRET_ACCESS_KEY").is_some());
        assert!(aws.get("aws_access_key_id").is_none());
    }

    #[test]
    fn aws_to_secret() {
        let aws = AwsCredentials {
            aws_access_key_id: String::from("xxx").into(),
            aws_secret_access_key: String::from("yyy").into(),
        };
        let creds = json::to_value(aws).unwrap();
        let mut creds = creds
            .as_object()
            .into_iter()
            .flatten()
            .map(|(key, value)| (key.to_string(), value.as_str().unwrap_or_default()));
        let (key, value) = creds.next().unwrap();
        assert_eq!(key, "AWS_ACCESS_KEY_ID");
        assert_eq!(value, "xxx");

        let (key, value) = creds.next().unwrap();
        assert_eq!(key, "AWS_SECRET_ACCESS_KEY");
        assert_eq!(value, "yyy");

        assert!(creds.next().is_none());
    }
}
