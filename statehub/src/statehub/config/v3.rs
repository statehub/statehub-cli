//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use indexmap::{indexmap, IndexMap};

use super::*;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub(crate) struct Config {
    #[serde(default = "Config::version")]
    version: String,
    #[serde(default = "Config::default_profile_name")]
    profile: String,
    #[serde(default = "Config::default_profiles", flatten)]
    profiles: IndexMap<String, Profile>,
}

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub(crate) struct Profile {
    api: Option<String>,
    console: Option<String>,
    token: Option<String>,
    color: Option<String>,
}

impl Config {
    const VERSION: &'static str = "3";

    fn version() -> String {
        Self::VERSION.to_string()
    }

    fn default_profile_name() -> String {
        "default".to_string()
    }

    fn default_profiles() -> IndexMap<String, Profile> {
        let profile = Profile::default();
        indexmap!["default".to_string() => profile]
    }

    pub(crate) fn api(&self, profile: Option<&str>) -> &str {
        let profile = profile.unwrap_or(&self.profile);
        self.profiles
            .get(profile)
            .and_then(|profile| profile.api())
            .unwrap_or(DEFAULT_API)
    }

    pub(crate) fn console(&self, profile: Option<&str>) -> &str {
        let profile = profile.unwrap_or(&self.profile);
        self.profiles
            .get(profile)
            .and_then(|profile| profile.console())
            .unwrap_or(DEFAULT_CONSOLE)
    }

    pub(crate) fn token(&self, profile: Option<&str>) -> Option<&str> {
        let profile = profile.unwrap_or(&self.profile);
        self.profiles
            .get(profile)
            .and_then(|profile| profile.token())
    }

    pub(crate) fn color(&self, profile: Option<&str>) -> Option<&str> {
        let profile = profile.unwrap_or(&self.profile);
        self.profiles
            .get(profile)
            .and_then(|profile| profile.color())
    }

    pub(super) fn validate_config(config: &toml::Value) -> anyhow::Result<Self> {
        let version = config.get("version").and_then(|version| version.as_str());
        anyhow::ensure!(version == Some(Self::VERSION), "Unknown version");
        let config = config.clone();
        toml::Value::try_into(config).context("Converting to ConfigV2")
    }

    pub(crate) fn set_api(&mut self, profile: Option<&str>, api: impl ToString) {
        let profile = profile.unwrap_or(&self.profile).to_string();
        self.profiles.entry(profile).or_default().api = Some(api.to_string());
    }

    pub(crate) fn set_console(&mut self, profile: Option<&str>, console: impl ToString) {
        let profile = profile.unwrap_or(&self.profile).to_string();
        self.profiles.entry(profile).or_default().console = Some(console.to_string());
    }

    pub(crate) fn set_token(&mut self, profile: Option<&str>, token: impl ToString) {
        let profile = profile.unwrap_or(&self.profile).to_string();
        self.profiles.entry(profile).or_default().token = Some(token.to_string());
    }

    pub(crate) fn set_profile(&mut self, profile: impl ToString) {
        self.profile = profile.to_string();
    }
}

impl Default for Config {
    fn default() -> Self {
        Self {
            version: Self::version(),
            profile: Self::default_profile_name(),
            profiles: Self::default_profiles(),
        }
    }
}

impl Profile {
    fn api(&self) -> Option<&str> {
        self.api.as_deref()
    }

    fn console(&self) -> Option<&str> {
        self.console.as_deref()
    }

    pub(crate) fn token(&self) -> Option<&str> {
        self.token.as_deref()
    }

    pub(crate) fn color(&self) -> Option<&str> {
        self.color.as_deref()
    }
}

impl From<super::ConfigV2> for Config {
    fn from(v2: super::ConfigV2) -> Self {
        let version = Self::VERSION.to_string();
        let api = Some(v2.api().to_string());
        let console = Some(DEFAULT_CONSOLE.to_string());
        let token = v2.token().map(ToString::to_string);
        let color = None;

        let profile = Profile {
            api,
            console,
            token,
            color,
        };

        Self {
            version,
            profile: "default".to_string(),
            profiles: indexmap!["default".to_string() => profile],
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn empty() {
        const CONFIG: &str = "";

        let config: Config = toml::from_str(CONFIG).unwrap();
        assert_eq!(config.version, "3");
        assert_eq!(config.profile, "default");
    }

    #[test]
    fn default() {
        let config = Config::default();
        let text = toml::to_string_pretty(&config).unwrap();
        assert_eq!(text, "version = '3'\nprofile = 'default'\n\n[default]\n");
    }

    #[test]
    fn with_default_profile() {
        const CONFIG: &str = "version = '3'\nprofile = 'default'\n\n[default]\napi = 'https://api.acme.acme'\ntoken = 'aaa'";
        let config: Config = toml::from_str(CONFIG).unwrap();
        assert_eq!(config.api(None), "https://api.acme.acme");
        assert_eq!(config.token(None), Some("aaa"));
    }
    #[test]

    fn with_non_default_profile() {
        const CONFIG: &str = r#"
        version = '3'
        profile = 'two'

        [one]
        api = 'https://one.com'
        token = 'one'
        [two]
        api = 'https://two.com'
        token = 'two'
        "#;
        let config: Config = toml::from_str(CONFIG).unwrap();
        assert_eq!(config.api(None), "https://two.com");
        assert_eq!(config.token(None), Some("two"));
        assert_eq!(config.api(Some("one")), "https://one.com");
        assert_eq!(config.token(Some("one")), Some("one"));
    }
}
