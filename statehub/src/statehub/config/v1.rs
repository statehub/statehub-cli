//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use super::*;

#[derive(Clone, Debug, Serialize, Deserialize)]

pub(crate) struct Config {
    version: String,
    api: String,
    token: Option<String>,
}

impl Config {
    const VERSION: &'static str = "1";

    pub(super) fn validate_config(config: &toml::Value) -> anyhow::Result<Self> {
        let version = config.get("version").and_then(|version| version.as_str());
        anyhow::ensure!(version == Some(Self::VERSION), "Unknown version");
        let config = config.clone();
        toml::Value::try_into(config).context("Converting to v1::Config")
    }

    pub(crate) fn api(&self) -> &str {
        &self.api
    }

    pub(crate) fn token(&self) -> Option<&str> {
        self.token.as_deref()
    }
}
