//
// Copyright (c) 2022 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use std::collections::HashMap;
use std::fmt;
use std::io;
use std::str;

use clap::Parser;
use console::{Style, Term};
use dialoguer::{theme, Confirm, Input, MultiSelect, Select};
use futures::StreamExt;
use itertools::Itertools;
use serde::{de, Deserialize, Serialize};
use statehub_cluster_operator::StatehubCluster;

use crate::api;
use crate::k8s;
use crate::location::{AwsRegion, AzureRegion, Location};
use crate::show::{Detailed, Quiet};
use crate::traits::Show;
use crate::v0;
use crate::Output;

use aws::AwsCredentials;
use azure::AzureCredentials;
use config::Config;
use helper::{AddLocation, RegisterCluster};
use print::{ClusterAndStates, Print, StateAndClusters};

mod aws;
mod azure;
mod config;
mod events;
mod helper;
mod print;

const ABOUT: &str = "statehub CLI tool";

#[derive(Debug, clap::Parser)]
#[clap(about = ABOUT, version)]
pub(crate) struct Cli {
    #[clap(
        help = "Management server URL or address",
        // default_value = "https://api.statehub.io",
        long,
        short,
        global = true,
        env = "SHAPI"
    )]
    management: Option<String>,
    #[clap(
        help = "Console server URL or address",
        // default_value = "https://console.statehub.io",
        long,
        global = true,
        env = "SHCONSOLE"
    )]
    console: Option<String>,
    #[clap(help = "Authentication token", long, global = true, env = "SHTOKEN")]
    token: Option<String>,
    #[clap(long, global = true, help = "Show results as JSON")]
    json: bool,
    #[clap(
        help = "Configuration profile to use",
        long,
        global = true,
        env = "SHPROFILE"
    )]
    profile: Option<String>,
    #[clap(short, long, global = true)]
    verbose: bool,
    #[clap(subcommand)]
    command: Command,
}

#[derive(Debug, PartialEq, clap::Subcommand)]
enum Command {
    #[clap(about = "Authenticate against statehub service", display_order(0))]
    Login,

    #[clap(about = "Get entity info")]
    Get { entity: Entity },

    #[clap(
        about = "Create new state",
        visible_aliases = &["create-st", "cs"],
        display_order(20),
    )]
    CreateState {
        #[clap(help = "State name")]
        name: v0::StateName,
        #[clap(long, short, help = "Default owning cluster")]
        owner: Option<v0::ClusterName>,
        #[clap(long, short, help = "Location definition", alias = "location")]
        locations: Vec<Location>,
        #[clap(
            long,
            short,
            help = "Get locations from this cluster",
            alias = "cluster"
        )]
        clusters: Vec<v0::ClusterName>,
        #[clap(
            long,
            help = "Storage class name if different from the state name",
            visible_alias = "sc"
        )]
        storage_class: Option<String>,
        #[clap(
            long,
            help = "Default filesystem for volumes of this state",
            visible_alias = "fs"
        )]
        fs_type: Option<v0::VolumeFileSystem>,
        #[clap(
            long,
            help = "Extra query parameters for create_state API call",
            parse(try_from_str = parse_params),
            alias = "param",
            hide = true,
            multiple_values(true),
        )]
        params: Vec<(String, String)>,
    },

    #[clap(
        about = "Delete existing state",
        visible_aliases = &["delete-st", "ds"],
        display_order(20),
    )]
    DeleteState {
        #[clap(help = "State name")]
        name: v0::StateName,
    },

    #[clap(
        about = "List available states",
        visible_aliases = &["list-state", "list-st", "ls"],
        display_order(20),
    )]
    ListStates,

    #[clap(
        about = "Show state details",
        visible_aliases = &["show-s", "ss"],
        display_order(20),
    )]
    ShowState {
        #[clap(help = "State name")]
        name: v0::StateName,
    },

    #[clap(
        about = "Register new cluster",
        visible_aliases = &["register-cl", "rc"],
        display_order(10),
    )]
    RegisterCluster {
        #[clap(
            help = "Non-interactive cluster registration, automatic defaults apply",
            long,
            short = 'y',
            visible_alias = "yes"
        )]
        non_interactive: bool,

        #[clap(help = "Cluster name, defaults to current k8s context")]
        names: Vec<v0::ClusterName>,

        #[clap(
            help = "Kubeconfig context to use for registering cluster",
            long,
            short,
            visible_aliases = &["context", "ctx"]
        )]
        contexts: Vec<String>,

        #[clap(
            help = "List of states to for this cluster to use",
            long,
            visible_alias = "state",
            default_value = "default"
        )]
        states: Vec<v0::StateName>,

        #[clap(
            help = "Skip adding this cluster locations to any state",
            long,
            conflicts_with = "states"
        )]
        no_state: bool,

        #[clap(
            help = "Skip setting up default storage class",
            long,
            conflicts_with = "default-storage-class"
        )]
        no_default_storage_class: bool,

        #[clap(
            help = "The name of the state to configure as default storage class",
            long,
            conflicts_with_all = &["no-state", "no-default-storage-class"],
            visible_alias = "default-sc",
        )]
        default_storage_class: Option<String>,

        #[clap(help = "Do not register this cluster as state owner", long)]
        no_state_owner: bool,

        #[clap(
            help = "Namespace to install statehub components",
            long,
            default_value = "statehub-system",
            visible_alias = "ns"
        )]
        namespace: String,

        #[clap(help = "Skip running 'helm install'", long)]
        skip_helm: bool,

        #[clap(help = "K8s cluster provider [default: autodetect]", long)]
        provider: Option<v0::Provider>,

        #[clap(help = "Cleanup grace time out", long, default_value = "600s")]
        cleanup_grace: String,

        #[clap(flatten)]
        aws: AwsCredentials,

        #[clap(flatten)]
        azure: AzureCredentials,
    },

    #[clap(
        about = "Unregister existing cluster",
        visible_aliases = &["unregister-cl", "urc", "uc"],
        display_order(11),
    )]
    UnregisterCluster {
        #[clap(help = "Skip confirmation", long, short)]
        force: bool,

        #[clap(
            help = "Kubeconfig context to use for unregistering cluster",
            long,
            short,
            visible_alias = "ctx"
        )]
        context: Option<String>,

        #[clap(help = "Cluster name")]
        name: Option<v0::ClusterName>,

        #[clap(
            help = "Non-interactive cluster unregistration, automatic defaults apply",
            long,
            short = 'y',
            alias = "yes",
            hide = true
        )]
        non_interactive: bool,
    },

    #[clap(
        about = "Update registered cluster",
        visible_aliases = &["update-cl", "upc"],
        display_order(12),
    )]
    UpdateCluster {
        #[clap(help = "Cluster name")]
        name: Option<v0::ClusterName>,
        #[clap(
            help = "Namespace where statehub components are installed",
            long,
            default_value = "statehub-system"
        )]
        namespace: String,
        #[clap(flatten)]
        aws: AwsCredentials,
        #[clap(flatten)]
        azure: AzureCredentials,
        #[clap(
            help = "Kubeconfig context to use for updating cluster",
            long,
            short,
            visible_alias = "ctx"
        )]
        context: Option<String>,
    },

    #[clap(
        about = "List registered clusters",
        visible_aliases = &["list-cluster", "list-cl", "lc"],
        display_order(13),
    )]
    ListClusters,

    #[clap(
        about = "Show registered cluster details",
        visible_aliases = &["show-cl", "sc"],
        display_order(14),
    )]
    ShowCluster {
        #[clap(help = "Cluster name")]
        name: Option<v0::ClusterName>,
    },

    #[clap(
        about = "Diagnose registered cluster for any irregularities",
        visible_aliases = &["diagnose-cl", "dc"],
        display_order(15),
    )]
    DiagnoseCluster {
        #[clap(
            help = "Kubeconfig context to use for installing Statehub Cluster CRD",
            long,
            short
        )]
        context: Option<String>,
    },

    #[clap(
        about = "Manually make state available in a specified location",
        visible_aliases = &["add-l", "al"],
        display_order(30),
    )]
    AddLocation {
        #[clap(help = "State name")]
        state: v0::StateName,
        #[clap(
            help = "Location specification",
            conflicts_with = "cluster",
            required_unless_present = "cluster"
        )]
        location: Option<Location>,
        #[clap(
            help = "Add locations from this cluster",
            long,
            short,
            conflicts_with = "location",
            required_unless_present = "location"
        )]
        cluster: Option<v0::ClusterName>,
        #[clap(help = "Wait until new location is ready", long)]
        wait: bool,
    },

    #[clap(
        about = "Manually make state unavailable in a specified location",
        visible_aliases = &["remove-l", "rem-l", "rl"],
        display_order(30),
    )]
    RemoveLocation {
        #[clap(help = "State name")]
        state: v0::StateName,
        #[clap(help = "Location specification")]
        location: Location,
    },

    #[clap(about = "Set state availability grade", display_order(40), hide(true))]
    SetAvailability,

    #[clap(about = "Set cluster as the state owner", display_order(40))]
    SetOwner {
        #[clap(help = "State name")]
        state: v0::StateName,
        #[clap(help = "Cluster name")]
        cluster: v0::ClusterName,
    },

    #[clap(about = "Clear state owner", display_order(40))]
    UnsetOwner {
        #[clap(help = "State name")]
        state: v0::StateName,
        #[clap(help = "Cluster name")]
        cluster: v0::ClusterName,
    },

    #[clap(
        about = "Manually create new volume",
        visible_aliases = &["create-v", "cv"],
        display_order(50),
        hide(true),
    )]
    CreateVolume {
        #[clap(help = "State name")]
        state: v0::StateName,
        #[clap(help = "Volume name")]
        volume: v0::VolumeName,
        #[clap(help = "valume size")]
        size: u64,
        #[clap(help = "volume file system")]
        fs_type: v0::VolumeFileSystem,
    },

    #[clap(
        about = "Manually delete existing volume",
        visible_aliases = &["delete-v", "dv"],
        display_order(50),
        hide(true),
    )]
    DeleteVolume {
        #[clap(help = "State name")]
        state: v0::StateName,
        #[clap(help = "Volume name")]
        volume: v0::VolumeName,
        #[clap(help = "Wait until volume is deleted", long)]
        wait: bool,
    },

    #[clap(
        about = "Manually choose primary location for volume",
        visible_aliases = &["set-p", "sp"],
        display_order(50),
        hide(true),
    )]
    SetPrimary {
        #[clap(help = "State name")]
        state: v0::StateName,
        #[clap(help = "Volume name")]
        volume: v0::VolumeName,
        #[clap(help = "Primary location specification", long, short)]
        primary: Location,
    },

    #[clap(
        about = "List volumes in the given state",
        visible_aliases = &["list-volume", "list-v", "lv"],
        display_order(50),
    )]
    ListVolumes {
        #[clap(help = "State name")]
        state: v0::StateName,
    },

    #[clap(
        about = "Show volume[s] info in the given state",
        visible_aliases = &["show-volume", "show-v", "sv"],
        display_order(50),
    )]
    ShowVolumes {
        #[clap(help = "State name")]
        state: v0::StateName,
        #[clap(help = "Volume name")]
        volumes: Vec<v0::VolumeName>,
    },

    #[clap(
        about = "Uninstall statehub objects from the cluster",
        visible_aliases = &["uninstall-sh", "us"],
        display_order(50)
    )]
    UninstallStatehub {
        #[clap(help = "Skip confirmation", long, short)]
        force: bool,
        #[clap(help = "Cluster name")]
        name: v0::ClusterName,
        #[clap(
            help = "Kubeconfig context to use for uninstalling statehub",
            long,
            short,
            visible_alias = "ctx"
        )]
        context: Option<String>,
    },

    #[clap(
        about = "Just register named cluster via API (no interaction with K8s cluster",
        visible_alias = "jrc",
        display_order(1000),
        hide(true)
    )]
    JustRegisterCluster {
        #[clap(help = "Cluster name")]
        name: v0::ClusterName,
        #[clap(
            help = "Namespace to install statehub components",
            long,
            default_value = "statehub-system",
            visible_alias = "ns"
        )]
        namespace: String,
        #[clap(help = "K8s cluster provider", long, default_value = "generic")]
        provider: v0::Provider,
        #[clap(long, short, help = "Location definition", alias = "location")]
        locations: Vec<Location>,
    },

    #[clap(
        about = "Just unregister named cluster via API (no interaction with K8s cluster)",
        visible_aliases = &["jurc", "juc"],
        display_order(1000),
        hide(true)
    )]
    JustUnregisterCluster {
        #[clap(help = "Cluster name")]
        name: v0::ClusterName,
    },

    #[clap(
        about = "Save cluster token",
        visible_alias = "sct",
        display_order(1000),
        hide(true)
    )]
    SaveClusterToken {
        #[clap(help = "Namespace name")]
        namespace: String,
        #[clap(help = "Cluster token")]
        cluster_token: String,
        #[clap(help = "Kubeconfig context to use", long, short, visible_alias = "ctx")]
        context: Option<String>,
    },

    #[clap(
        about = "Setup statehub configmap",
        visible_aliases = &["scm", "setup-cm"],
        display_order(1000),
        hide(true)
    )]
    SetupConfigmap {
        #[clap(help = "cluster name")]
        cluster: v0::ClusterName,
        #[clap(help = "Kubeconfig context to use", long, short, visible_alias = "ctx")]
        context: Option<String>,
        #[clap(
            help = "Namespace to install statehub components",
            default_value = "statehub-system"
        )]
        namespace: String,
        #[clap(
            help = "The name of the state to configure as default storage class, nothing by default"
        )]
        default_state: Option<String>,
        #[clap(
            help = "The name of the state to configure as default storage class, nothing by default",
            default_value = "600s"
        )]
        cleanup_grace: String,
    },

    #[clap(
        about = "Install Statehub Cluster CRD",
        visible_aliases = &["icc", "crd"],
        display_order(1000),
        hide(true)
    )]
    InstallClusterCrd {
        #[clap(help = "cluster name")]
        cluster: v0::ClusterName,
        #[clap(
            help = "Kubeconfig context to use for installing Statehub Cluster CRD",
            long,
            short
        )]
        context: Option<String>,
        #[clap(
            help = "Namespace to install statehub components",
            default_value = "statehub-system"
        )]
        namespace: String,
    },

    #[clap(
        about = "Uninstall Statehub Cluster CRD",
        visible_aliases = &["ucc", "uncrd"],
        display_order(1000),
        hide(true)
    )]
    UninstallClusterCrd {
        #[clap(help = "cluster name")]
        cluster: v0::ClusterName,
        #[clap(
            help = "Kubeconfig context to use for uninstalling Statehub Cluster CRD",
            long,
            short
        )]
        context: Option<String>,
    },

    #[clap(
        about = "List K8s namespaces",
        visible_alias = "list-ns",
        display_order(1000),
        hide(true)
    )]
    ListNamespaces {
        #[clap(help = "Kubeconfig context to use", long, short, visible_alias = "ctx")]
        context: Option<String>,
    },

    #[clap(about = "List K8s nodes", display_order(1000), hide(true))]
    ListNodes {
        #[clap(help = "Kubeconfig context to use", long, short, visible_alias = "ctx")]
        context: Option<String>,
    },

    #[clap(about = "List K8s pods", display_order(1000), hide(true))]
    ListPods {
        #[clap(help = "Kubeconfig context to use", long, short, visible_alias = "ctx")]
        context: Option<String>,
    },

    #[clap(
        about = "List K8s node regions",
        visible_aliases = &["list-r", "lr"],
        display_order(1000),
        hide(true)
    )]
    ListRegions {
        #[clap(help = "Kubeconfig context to use", long, short, visible_alias = "ctx")]
        context: Option<String>,
        #[clap(help = "Also list zones", long, short)]
        zone: bool,
    },

    #[clap(about = "Save default configuration file", display_order(2000))]
    SaveConfig {
        #[clap(help = "Save selected profile as default profile", long, short = 'd')]
        set_default_profile: bool,
    },

    #[clap(
        about = "Check for updates to statehub utility",
        visible_alias = "up",
        display_order(3000)
    )]
    SelfUpdate,
}

#[derive(Clone, Copy, Debug, PartialEq)]
enum Entity {
    Clusters,
    States,
}

impl Command {
    fn requires_api_access(&self) -> bool {
        match self {
            Self::Login => false,
            Self::Get { .. } => true,
            Self::CreateState { .. } => true,
            Self::DeleteState { .. } => true,
            Self::ListStates => true,
            Self::ShowState { .. } => true,
            Self::RegisterCluster { .. } => true,
            Self::UnregisterCluster { .. } => true,
            Self::UpdateCluster { .. } => true,
            Self::ListClusters => true,
            Self::ShowCluster { .. } => true,
            Self::DiagnoseCluster { .. } => true,
            Self::AddLocation { .. } => true,
            Self::RemoveLocation { .. } => true,
            Self::SetAvailability => true,
            Self::SetOwner { .. } => true,
            Self::UnsetOwner { .. } => true,
            Self::CreateVolume { .. } => true,
            Self::DeleteVolume { .. } => true,
            Self::SetPrimary { .. } => true,
            Self::ListVolumes { .. } => true,
            Self::ShowVolumes { .. } => true,
            Self::UninstallStatehub { .. } => false,
            Self::JustRegisterCluster { .. } => true,
            Self::JustUnregisterCluster { .. } => true,
            Self::SaveClusterToken { .. } => false,
            Self::SetupConfigmap { .. } => false,
            Self::InstallClusterCrd { .. } => false,
            Self::UninstallClusterCrd { .. } => false,
            Self::ListNamespaces { .. } => false,
            Self::ListNodes { .. } => false,
            Self::ListPods { .. } => false,
            Self::ListRegions { .. } => false,
            Self::SaveConfig { .. } => false,
            Self::SelfUpdate => false,
        }
    }
}

impl Cli {
    pub(crate) async fn execute() -> super::CliResult {
        let cli = Self::parse();
        let json = cli.json;

        let result = cli.dispatch().await;
        super::CliResult::new(json, result)

        // .or_else(|err| if json { json_error(err) } else { Err(err) })
    }

    async fn config(&self) -> anyhow::Result<Config> {
        let profile = self.profile.clone();
        let config = Config::load(profile).unwrap_or_default();
        Ok(config)
    }

    async fn dispatch(self) -> anyhow::Result<()> {
        let config = self
            .config()
            .await?
            .optionally_management_api(self.management)
            .optionally_management_console(self.console)
            .set_token(self.token);

        let statehub = StateHub::new(config, self.json, self.verbose);

        statehub.check_for_update().await;
        if self.command.requires_api_access() {
            statehub.validate_auth().await?;
        }

        match self.command {
            Command::Login => statehub.login().await,
            Command::Get { entity } => match entity {
                Entity::Clusters => statehub.list_clusters().await,
                Entity::States => statehub.list_states().await,
            },
            Command::CreateState {
                name,
                owner,
                locations,
                clusters,
                storage_class,
                fs_type,
                params,
            } => {
                let storage_class =
                    v0::StorageClass::new_if_not_default(&name, storage_class, fs_type);
                statehub
                    .create_state(name, owner, locations, clusters, storage_class, params)
                    .await
            }
            Command::DeleteState { name } => statehub.delete_state(name).await,
            Command::ListStates => statehub.list_states().await,
            Command::ShowState { name } => statehub.show_state(&name).await,
            Command::RegisterCluster {
                non_interactive,
                names,
                contexts,
                states,
                no_state,
                no_default_storage_class,
                default_storage_class,
                no_state_owner,
                namespace,
                skip_helm,
                provider,
                cleanup_grace,
                aws,
                azure,
            } => {
                aws.validate()?;
                azure.validate()?;
                let (kubectls, register_cluster) = if non_interactive {
                    statehub
                        .non_interactive_register_cluster_helper(
                            names,
                            contexts,
                            states,
                            no_state,
                            no_default_storage_class,
                            default_storage_class,
                            no_state_owner,
                            namespace,
                            skip_helm,
                            provider,
                            cleanup_grace,
                            aws,
                            azure,
                        )
                        .await?
                } else {
                    statehub
                        .interactive_register_cluster_helper(
                            names,
                            contexts,
                            states,
                            no_state,
                            no_default_storage_class,
                            default_storage_class,
                            no_state_owner,
                            namespace,
                            skip_helm,
                            provider,
                            cleanup_grace,
                            aws,
                            azure,
                        )
                        .await?
                };

                statehub
                    .register_multiple_clusters(kubectls, register_cluster)
                    .await
            }
            Command::UnregisterCluster {
                force,
                context,
                name,
                non_interactive,
            } => {
                let names = name.into_iter().collect();
                let contexts = context.into_iter().collect();
                let kubectls = k8s::Kubectl::with_names_and_contexts(names, contexts)?;
                let helm = k8s::Helm::skip();

                statehub
                    .unregister_cluster(kubectls, helm, force, non_interactive)
                    .await
            }
            Command::UpdateCluster {
                name,
                namespace,
                aws,
                azure,
                context,
            } => {
                // let name = name.or_else(k8s::get_current_cluster_name).ok_or_else(|| {
                //     anyhow::anyhow!(
                //         "No default Kubernetes context found, need to provide cluster name"
                //     )
                // })?;
                // let kubectl = k8s::Kubectl::with_context_or_current(None)?;
                aws.validate()?;
                azure.validate()?;
                statehub
                    .update_cluster(name, context, namespace, aws, azure)
                    .await
            }
            Command::ListClusters => statehub.list_clusters().await,
            Command::ShowCluster { name } => {
                let name = name.or_else(k8s::get_current_cluster_name).ok_or_else(|| {
                    anyhow::anyhow!(
                        "No default Kubernetes context found, need to provide cluster name"
                    )
                })?;
                statehub.show_cluster(name).await
            }
            Command::DiagnoseCluster { context } => {
                let names = vec![];
                let contexts = context.into_iter().collect();
                let kubectls = k8s::Kubectl::with_names_and_contexts(names, contexts)?;
                statehub.diagnose_cluster(kubectls).await
            }
            Command::AddLocation {
                state,
                location,
                cluster,
                wait,
            } => {
                let location = location.map(AddLocation::FromLocation);
                let cluster = cluster.map(AddLocation::FromCluster);
                if let Some(add_location) = location.or(cluster) {
                    statehub.add_location(state, add_location, wait).await
                } else {
                    anyhow::bail!("Need to specify either location or --cluster");
                }
            }
            Command::RemoveLocation { state, location } => {
                statehub.remove_location(state, location).await
            }
            Command::SetAvailability => statehub.set_availability().await,
            Command::SetOwner { state, cluster } => statehub.set_owner(state, cluster).await,
            Command::UnsetOwner { state, cluster } => statehub.unset_owner(state, cluster).await,
            Command::CreateVolume {
                state,
                volume,
                size,
                fs_type,
            } => statehub.create_volume(state, volume, size, fs_type).await,
            Command::DeleteVolume {
                state,
                volume,
                wait,
            } => statehub.delete_volume(state, volume, wait).await,
            Command::SetPrimary {
                state,
                volume,
                primary,
            } => statehub.set_volume_primary(state, volume, primary).await,
            Command::ListVolumes { state } => statehub.list_volumes(state).await,
            Command::ShowVolumes { state, volumes } => statehub.show_volumes(state, volumes).await,
            Command::UninstallStatehub {
                force,
                name,
                context,
            } => {
                let names = vec![name];
                let contexts = context.into_iter().collect();
                let kubectls = k8s::Kubectl::with_names_and_contexts(names, contexts)?;

                statehub.uninstall_statehub(kubectls, force).await
            }
            Command::JustRegisterCluster {
                name,
                namespace,
                provider,
                locations,
            } => {
                statehub
                    .just_register_cluster(name, namespace, provider, locations)
                    .await
            }
            Command::JustUnregisterCluster { name } => statehub.just_unregister_cluster(name).await,
            Command::SaveClusterToken {
                namespace,
                cluster_token,
                context,
            } => {
                statehub
                    .save_cluster_token(namespace, cluster_token, context)
                    .await
            }
            Command::SetupConfigmap {
                cluster,
                context,
                namespace,
                default_state,
                cleanup_grace,
            } => {
                statehub
                    .setup_configmap(cluster, context, namespace, default_state, cleanup_grace)
                    .await
            }
            Command::InstallClusterCrd {
                cluster,
                context,
                namespace,
            } => {
                let names = vec![cluster];
                let contexts = context.into_iter().collect();
                let kubectls = k8s::Kubectl::with_names_and_contexts(names, contexts)?;
                statehub.install_cluster_crd(kubectls, namespace).await
            }
            Command::UninstallClusterCrd { cluster, context } => {
                let names = vec![cluster];
                let contexts = context.into_iter().collect();
                let kubectls = k8s::Kubectl::with_names_and_contexts(names, contexts)?;
                statehub.uninstall_cluster_crd(kubectls).await
            }
            Command::ListNamespaces { context } => statehub.list_namespaces(context).await,
            Command::ListNodes { context } => statehub.list_nodes(context).await,
            Command::ListPods { context } => statehub.list_pods(context).await,
            Command::ListRegions { context, zone } => statehub.list_regions(context, zone).await,
            Command::SaveConfig {
                set_default_profile,
            } => statehub.save_config(set_default_profile).await,
            Command::SelfUpdate => statehub.self_update().await,
        }
    }
}

pub(crate) struct StateHub {
    config: Config,
    api: api::Api,
    stdout: Term,
    stderr: Term,
    style: Style,
    theme: theme::SimpleTheme,
    json: bool,
    verbose: bool,
}

impl StateHub {
    const PLATFORM: &'static str = env!("VERGEN_CARGO_TARGET_TRIPLE");
    const VERSION: &'static str = env!("VERGEN_BUILD_SEMVER");

    fn new(config: Config, json: bool, verbose: bool) -> Self {
        let api = api::Api::new(config.api(), v0::VERSION, config.token());
        let stdout = Term::stdout();
        let stderr = Term::stderr();
        let theme = theme::SimpleTheme;
        let style = config.style();

        Self {
            config,
            api,
            stdout,
            stderr,
            style,
            theme,
            json,
            verbose,
        }
    }

    async fn validate_auth(&self) -> anyhow::Result<()> {
        if self.api.is_unauthorized().await {
            // log::error!("Unauthorized - perhaps an invalid token?");
            anyhow::bail!(
                "Unauthorized (Invalid token or no token)\nTo correct run\n\n    statehub login"
            );
        }

        Ok(())
    }

    async fn login(&self) -> anyhow::Result<()> {
        let console = self.config.console();
        let (token, id) = self.login_prompt_helper()?;

        let prompt = format!(
            "{}\n{}\n\n{}\n{}",
            "Welcome to Statehub! Please login or sign up at:",
            format_args!("{}/?cli={}", console, token),
            "Waiting for authentication to complete in your browser...",
            format_args!(
                "Please paste the token generated for {} here, and press RETURN",
                id
            )
        );

        let token = self.input(prompt)?;

        self.config.clone().set_token(Some(token)).save(false)?;
        Ok(())
    }

    async fn create_state(
        &self,
        name: v0::StateName,
        owner: Option<v0::ClusterName>,
        locations: Vec<Location>,
        clusters: Vec<v0::ClusterName>,
        storage_class: Option<v0::StorageClass>,
        params: Vec<(String, String)>,
    ) -> anyhow::Result<()> {
        let mut locations = vec![locations];
        for name in &clusters {
            locations.push(self.api.get_cluster(name).await?.all_locations());
        }
        let locations = locations.into_iter().flatten().unique().collect();
        let state = v0::CreateStateDto {
            name,
            storage_class,
            owner,
            locations,
            allowed_clusters: None,
        };
        self.api
            .create_state(state, params)
            .await
            .print(&self.stdout, &self.style, self.json)
    }

    async fn delete_state(&self, name: v0::StateName) -> anyhow::Result<()> {
        self.api
            .delete_state(name)
            .await
            .print(&self.stdout, &self.style, self.json)
    }

    async fn show_state(self, state: &v0::StateName) -> anyhow::Result<()> {
        let state = self.api.get_state(state).await.map(Detailed)?;
        if let Ok(clusters) = self.api.get_all_clusters().await {
            StateAndClusters::new(state, clusters).print(&self.stdout, &self.style, self.json)
        } else {
            state.print(&self.stdout, &self.style, self.json)
        }
    }

    async fn list_states(&self) -> anyhow::Result<()> {
        self.api
            .get_all_states()
            .await
            .print(&self.stdout, &self.style, self.json)
    }

    async fn list_clusters(&self) -> anyhow::Result<()> {
        self.api
            .get_all_clusters()
            .await
            .print(&self.stdout, &self.style, self.json)
    }

    async fn register_multiple_clusters(
        &self,
        kubectls: Vec<k8s::Kubectl>,
        register_cluster: RegisterCluster,
    ) -> anyhow::Result<()> {
        let mut clusters = indexmap::indexmap!();

        for kubectl in kubectls {
            let cluster_locations = kubectl.collect_node_locations().await?;
            // Ideally I would store it as (kubectl => locations),
            // but Kubeconfig doesn't impl Hash, so I cannot derive it on Kubectl
            clusters.insert(cluster_locations, kubectl);
        }

        let locations = clusters
            .keys()
            .flatten()
            .unique()
            .copied()
            .collect::<Vec<_>>();

        if let Some(states) = register_cluster.states() {
            self.adjust_all_states(states, &locations, false).await?;
        } else {
            self.verbosely("Skip adding this cluster to any state")?;
        }

        for (locations, kubectl) in clusters {
            self.register_cluster_helper(kubectl, &locations, &register_cluster)
                .await?;
        }

        Ok(())
    }

    async fn update_cluster(
        &self,
        name: Option<v0::ClusterName>,
        context: Option<String>,
        namespace: String,
        aws: AwsCredentials,
        azure: AzureCredentials,
    ) -> anyhow::Result<()> {
        let names = name.into_iter().collect();
        let contexts = context.into_iter().collect();
        let kubectls = k8s::Kubectl::with_names_and_contexts(names, contexts)?;

        for kubectl in kubectls {
            let name = kubectl.cluster_name();
            self.inform(format_args!(
                "Updating cluster {name} (kubeconfig context {})",
                kubectl.context()
            ));
            let statehub_cluster = StatehubCluster::with_namespace(name, &namespace);
            self.setup_credentials_helper(&kubectl, &statehub_cluster, &aws, &azure)
                .await?;
        }
        Ok(())
    }

    async fn show_cluster(&self, name: v0::ClusterName) -> anyhow::Result<()> {
        let cluster = self.api.get_cluster(&name).await.map(Detailed)?;
        if let Ok(states) = self.api.get_all_states().await {
            ClusterAndStates::new(cluster, states).print(&self.stdout, &self.style, self.json)
        } else {
            cluster.print(&self.stdout, &self.style, self.json)
        }
    }

    async fn diagnose_cluster(&self, kubectls: Vec<k8s::Kubectl>) -> anyhow::Result<()> {
        for kubectl in kubectls {
            self.inform(format_args!(
                "Running diagnostics on cluster '{}' (K8s context '{}')",
                kubectl.cluster_name(),
                kubectl.context(),
            ));
            let check_cluster = kubectl.check_cluster().await;
            futures::pin_mut!(check_cluster);
            while let Some(item) = check_cluster.next().await {
                let text = item?;
                self.inform(text);
            }
        }
        Ok(())
    }

    async fn unregister_cluster(
        &self,
        kubectls: Vec<k8s::Kubectl>,
        helm: k8s::Helm,
        force: bool,
        non_interactive: bool,
    ) -> anyhow::Result<()> {
        if force || non_interactive || self.confirm_with_default("Are you sure?", false) {
            self.inform("Make sure all the pods using statehub are terminated");
            self.inform("And then manually run 'helm uninstall'");
            for kubectl in kubectls {
                self.unregister_cluster_helper(kubectl, helm).await?;
            }
        }
        Ok(())
    }

    async fn just_register_cluster(
        &self,
        name: v0::ClusterName,
        namespace: String,
        provider: v0::Provider,
        locations: Vec<Location>,
    ) -> anyhow::Result<()> {
        self.api
            .register_cluster(&name, &namespace, provider, &locations)
            .await
            .print(&self.stdout, &self.style, self.json)
    }

    async fn just_unregister_cluster(&self, name: v0::ClusterName) -> anyhow::Result<()> {
        self.api
            .unregister_cluster(&name)
            .await
            .print(&self.stdout, &self.style, self.json)
    }

    async fn add_location(
        &self,
        state: v0::StateName,
        add_location: AddLocation,
        wait: bool,
    ) -> anyhow::Result<()> {
        let locations = match add_location {
            AddLocation::FromLocation(location) => vec![location],
            AddLocation::FromCluster(cluster) => {
                let locations = self.api.get_cluster(&cluster).await?.all_locations();
                self.inform(format_args!(
                    "Cluster {} available in {}",
                    cluster,
                    locations.iter().map(ToString::to_string).join(" and ")
                ));
                locations
            }
        };

        let wait = (locations.len() > 1) || wait;

        for location in locations {
            let state = self.api.get_state(&state).await?;
            if state.is_available_in(&location) {
                self.inform(format_args!(
                    "State {} is already available in {:#}",
                    state, location
                ));
            } else {
                self.inform(format_args!("Extending state {} to {:#}", state, location));
                self.add_location_helper(&state, &location, wait).await?;
            }
        }
        Ok(())
    }

    async fn remove_location(self, state: v0::StateName, location: Location) -> anyhow::Result<()> {
        let state = self.api.get_state(&state).await?;
        if state.is_available_in(&location) {
            self.remove_location_helper(&state, &location).await?;
        } else {
            log::info!("{} is not availabe in {}", state, location);
        }
        Ok(())
    }

    async fn create_volume(
        self,
        state_name: v0::StateName,
        volume_name: v0::VolumeName,
        size: u64,
        fs: v0::VolumeFileSystem,
    ) -> anyhow::Result<()> {
        let volume = v0::CreateVolumeDto {
            name: volume_name.to_string(),
            size_gi: size,
            fs_type: fs.to_string(),
        };

        self.api
            .create_volume(state_name, volume)
            .await
            .map(Quiet)
            .print(&self.stdout, &self.style, self.json)
    }

    async fn delete_volume(
        self,
        state: v0::StateName,
        volume: v0::VolumeName,
        wait: bool,
    ) -> anyhow::Result<()> {
        if let Ok(volume) = self.api.get_volume(&state, &volume).await {
            if volume.is_deleting() {
                self.verbosely(format_args!(
                    "Volume {} is already being deleted",
                    volume.name
                ))?;
            } else {
                self.delete_volume_helper(&state, &volume.name, wait)
                    .await
                    .map(Quiet)
                    .print(&self.stdout, &self.style, self.json)?;
            }
        } else {
            self.verbosely("No such volume")?;
        }
        Ok(())
    }

    async fn set_volume_primary(
        &self,
        state: v0::StateName,
        volume: v0::VolumeName,
        primary: Location,
    ) -> anyhow::Result<()> {
        self.api
            .set_volume_primary(state, volume, primary)
            .await
            .print(&self.stdout, &self.style, self.json)
    }

    async fn list_volumes(&self, state: v0::StateName) -> anyhow::Result<()> {
        self.api
            .get_all_volumes(state)
            .await
            .print(&self.stdout, &self.style, self.json)
    }

    async fn show_volumes(
        &self,
        state: v0::StateName,
        volumes: Vec<v0::VolumeName>,
    ) -> anyhow::Result<()> {
        if volumes.is_empty() {
            self.api.get_all_volumes(state).await.map(Detailed).print(
                &self.stdout,
                &self.style,
                self.json,
            )
        } else {
            for volume in volumes {
                self.api
                    .get_volume(&state, &volume)
                    .await
                    .map(Detailed)
                    .print(&self.stdout, &self.style, self.json)?;
            }
            Ok(())
        }
    }

    async fn set_availability(self) -> anyhow::Result<()> {
        // Ok(Output::<String>::todo()).handle_output(&self.stdout, self.json)
        anyhow::bail!(self.show(Output::<String>::todo()))
    }

    async fn set_owner(
        &self,
        state: v0::StateName,
        cluster: v0::ClusterName,
    ) -> anyhow::Result<()> {
        self.api
            .set_owner(&state, &cluster)
            .await
            .print(&self.stdout, &self.style, self.json)
    }

    async fn unset_owner(
        &self,
        state: v0::StateName,
        cluster: v0::ClusterName,
    ) -> anyhow::Result<()> {
        let state = self.api.get_state(&state).await?;

        if state.owner == Some(cluster) {
            self.api
                .unset_owner(&state.name)
                .await
                .print(&self.stdout, &self.style, self.json)
        } else {
            anyhow::bail!("Permission denied, you are not theowner of this state.")
        }
    }

    pub(crate) fn show<T>(&self, output: Output<T>) -> String
    where
        T: de::DeserializeOwned + Serialize + Show,
    {
        output.into_text(self.json)
    }

    async fn uninstall_statehub(
        &self,
        kubectls: Vec<k8s::Kubectl>,
        force: bool,
    ) -> anyhow::Result<()> {
        if force || self.confirm_with_default("Are you sure?", false) {
            log::info!("Make sure all the pods using statehub are terminated");
            log::info!("Uninstall helm");
            for kubectl in kubectls {
                kubectl.cleanup_statehub_cluster().await?;
            }
        }
        Ok(())
    }

    async fn save_cluster_token(
        &self,
        namespace: String,
        token: String,
        context: Option<String>,
    ) -> anyhow::Result<()> {
        let secret = k8s::Kubectl::with_context_or_current(context.as_deref())?
            .store_cluster_token(&namespace, &token)
            .await?;
        if let Some(token) = k8s::extract_cluster_token(&secret) {
            log::debug!("Token: {}", token);
        }
        Ok(())
    }

    async fn setup_configmap(
        &self,
        cluster: v0::ClusterName,
        context: Option<String>,
        namespace: String,
        default_state: Option<String>,
        cleanup_grace: String,
    ) -> anyhow::Result<()> {
        let names = vec![cluster];
        let contexts = context.into_iter().collect();
        let kubectls = k8s::Kubectl::with_names_and_contexts(names, contexts)?;
        let default_state = default_state.as_deref().unwrap_or("");
        for kubectl in kubectls {
            let name = kubectl.cluster_name();
            let statehub_cluster = StatehubCluster::with_namespace(name, &namespace);
            self.setup_configmap_helper(&kubectl, &statehub_cluster, default_state, &cleanup_grace)
                .await?;
        }

        Ok(())
    }

    async fn install_cluster_crd(
        &self,
        kubectls: Vec<k8s::Kubectl>,
        namespace: String,
    ) -> anyhow::Result<()> {
        for kubectl in kubectls {
            kubectl.install_statehub_cluster(&namespace).await?;
        }

        Ok(())
    }

    async fn uninstall_cluster_crd(&self, kubectls: Vec<k8s::Kubectl>) -> anyhow::Result<()> {
        for kubectl in kubectls {
            kubectl.uninstall_statehub_cluster().await?;
        }

        Ok(())
    }

    async fn list_regions(&self, context: Option<String>, zone: bool) -> anyhow::Result<()> {
        k8s::Kubectl::with_context_or_current(context.as_deref())?
            .get_regions(zone)
            .await
            .map(|map| {
                map.into_iter()
                    .map(|(key, value)| (key.unwrap_or_default(), value))
                    .collect::<HashMap<_, _>>()
            })
            .map(Output::from)
            .print(&self.stdout, &self.style, self.json)
    }

    async fn list_namespaces(&self, context: Option<String>) -> anyhow::Result<()> {
        k8s::Kubectl::with_context_or_current(context.as_deref())?
            .list_namespaces()
            .await?
            .into_iter()
            .for_each(|namespace| println!("{:#?}", namespace));
        Ok(())
    }

    async fn list_nodes(&self, context: Option<String>) -> anyhow::Result<()> {
        k8s::Kubectl::with_context_or_current(context.as_deref())?
            .list_nodes()
            .await?
            .into_iter()
            .for_each(|node| println!("{}", node.show()));
        Ok(())
    }

    async fn list_pods(&self, context: Option<String>) -> anyhow::Result<()> {
        k8s::Kubectl::with_context_or_current(context.as_deref())?
            .list_pods()
            .await?
            .into_iter()
            .for_each(|pod| println!("{}", pod.show()));
        Ok(())
    }

    async fn save_config(&self, set_default_profile: bool) -> anyhow::Result<()> {
        self.config
            .save(set_default_profile)
            .map(|path| self.inform(format_args!("Saving config to {}", path.display())))
    }

    async fn check_for_update(&self) {
        if !self.json && Config::more_than_one_day_passed_since_last_check() {
            if let Some((current, latest)) = self.check_for_update_helper().await {
                self.inform(format_args!(
                    "\nNewer version '{}' is available (you have '{}')\n",
                    latest, current
                ))
            }
            Config::touch_update();
        }
    }

    async fn self_update(&self) -> anyhow::Result<()> {
        if let Some((current, latest)) = self.check_for_update_helper().await {
            if current < latest {
                self.update_helper(latest).await?;
            }
        }

        Ok(())
    }

    fn confirm_with_default(&self, prompt: impl Into<String>, yes: bool) -> bool {
        let theme = self.theme();
        Confirm::with_theme(theme)
            .with_prompt(prompt)
            .default(yes)
            .show_default(true)
            .interact()
            .unwrap_or(false)
    }

    fn theme(&self) -> &dyn theme::Theme {
        &self.theme
    }

    fn verbosely(&self, text: impl fmt::Display) -> io::Result<()> {
        if !self.json && self.verbose {
            self.stdout.write_line(&text.to_string())
        } else {
            Ok(())
        }
    }

    fn inform(&self, text: impl fmt::Display) {
        if !self.json {
            let text = format!("{}", self.style.apply_to(text));
            if !text.is_empty() {
                self.stdout.write_line(&text).ok();
            }
        }
    }

    fn error(&self, text: impl fmt::Display) -> io::Result<()> {
        let text = text.to_string();
        if !text.is_empty() {
            self.stderr.write_line(&text)?;
        }
        Ok(())
    }

    fn input(&self, prompt: impl Into<String>) -> io::Result<String> {
        Input::with_theme(self.theme())
            .with_prompt(prompt)
            .interact_text_on(&self.stdout)
    }

    fn input_with_default<T>(&self, prompt: impl Into<String>, default: T) -> io::Result<T>
    where
        T: Clone + str::FromStr + fmt::Display,
        T::Err: fmt::Display + fmt::Debug,
    {
        Input::with_theme(self.theme())
            .with_prompt(prompt)
            .default(default)
            .interact_text_on(&self.stdout)
    }

    fn select_with_default(
        &self,
        prompt: impl Into<String>,
        items: Vec<&str>,
        default: impl ToString,
    ) -> io::Result<String> {
        let items = items
            .into_iter()
            .map(ToString::to_string)
            .collect::<Vec<_>>();
        let default = default.to_string();
        let default = items.iter().position(|item| item == &default).unwrap_or(0);
        Select::with_theme(self.theme())
            .with_prompt(prompt)
            .items(&items)
            .default(default)
            .interact_on(&self.stdout)
            .map(|idx| items[idx].clone())
    }

    fn multiselect_with_default<P, T>(
        &self,
        prompt: P,
        items: &[T],
        default: usize,
    ) -> io::Result<Vec<T>>
    where
        P: Into<String>,
        T: Clone + ToString,
    {
        let items = items
            .iter()
            .enumerate()
            .map(|(idx, item)| (item.clone(), idx == default))
            .collect::<Vec<_>>();
        let items = MultiSelect::with_theme(self.theme())
            .with_prompt(prompt)
            .items_checked(&items)
            .interact_on(&self.stdout)?
            .into_iter()
            .map(|idx| items[idx].0.clone())
            .collect();

        Ok(items)
    }
}

impl str::FromStr for Entity {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.starts_with("cluster") {
            Ok(Self::Clusters)
        } else if s.starts_with("state") {
            Ok(Self::States)
        } else {
            anyhow::bail!("Supported entities are 'clusters' and 'states'")
        }
    }
}

fn parse_params(text: &str) -> anyhow::Result<(String, String)> {
    text.split('=')
        .collect_tuple()
        .map(|(key, value)| (key.to_string(), value.to_string()))
        .ok_or_else(|| anyhow::anyhow!("Invalid format: should be key=value"))
}
