//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use std::io;
use std::process::{Command, ExitStatus};

use tokio::process::Command as AsyncCmd;

use crate::traits::Show;
use crate::v0;

#[derive(Clone, Copy, Debug)]
pub(crate) enum Helm {
    Skip,
    Do,
}

impl Helm {
    pub(crate) fn new(skip_helm: bool) -> Self {
        if skip_helm {
            Self::Skip
        } else if helm_is_found() {
            Self::Do
        } else {
            log::warn!("helm is not detected. Will show helm command instead of executing them");
            Self::Skip
        }
    }

    pub(crate) fn skip() -> Self {
        Self::Skip
    }

    pub(crate) async fn execute(&self, commands: Vec<Command>) -> io::Result<(String, String)> {
        let (stdout, stderr) = match self {
            Helm::Skip { .. } => (
                format!("Manually run\n{}", commands.detailed_show()),
                String::new(),
            ),
            Helm::Do { .. } => {
                let mut success = String::new();
                let mut failure = String::new();
                for cmd in commands {
                    let input = cmd.show();
                    let (status, stdout, stderr) = self.exec(cmd).await?;
                    log::debug!("{}\n{}", input, stdout);
                    if status.success() {
                        success += &format!("{}\n{}\n", input, stdout);
                    } else {
                        failure += &format!("Running '{}' failed:\n{}\n", input, stderr);
                    }
                }
                (success, failure)
            }
        };
        Ok((stdout, stderr))
    }

    pub(crate) fn install_command(
        &self,
        cluster: &v0::Cluster,
        kube_context: Option<&str>,
        namespace: &str,
    ) -> Vec<Command> {
        cluster
            .helm
            .iter()
            .map(|helm| {
                let mut cmd = Command::new("helm");
                cmd.arg("upgrade")
                    .arg("--install")
                    .arg(&helm.chart)
                    .arg("--namespace")
                    .arg(namespace)
                    .arg("--repo")
                    .arg(&helm.repo)
                    .arg("--version")
                    .arg(&helm.version)
                    .arg(&helm.chart);
                for (param, value) in &helm.parameters {
                    cmd.arg("--set").arg(format!("{}={}", param, value));
                }
                if let Some(kube_context) = kube_context {
                    cmd.arg("--kube-context").arg(kube_context);
                }
                cmd
            })
            .collect()
    }

    pub(crate) fn uninstall_command(
        &self,
        cluster: &v0::Cluster,
        kube_context: Option<&str>,
        namespace: &str,
    ) -> Vec<Command> {
        cluster
            .helm
            .iter()
            .map(|helm| {
                let mut cmd = Command::new("helm");
                cmd.arg("uninstall")
                    .arg(&helm.chart)
                    .arg("--namespace")
                    .arg(namespace);
                if let Some(kube_context) = kube_context {
                    cmd.arg("--kube-context").arg(kube_context);
                }
                cmd
            })
            .collect()
    }

    async fn exec(&self, command: Command) -> io::Result<(ExitStatus, String, String)> {
        let output = AsyncCmd::from(command).output().await?;
        let stdout = String::from_utf8_lossy(&output.stdout).trim().to_string();
        let stderr = String::from_utf8_lossy(&output.stderr).trim().to_string();
        Ok((output.status, stdout, stderr))
    }
}

fn helm_is_found() -> bool {
    which::which("helm").is_ok()
}
