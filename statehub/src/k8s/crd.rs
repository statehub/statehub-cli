//
// Copyright (c) 2021 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use kube::CustomResourceExt;

use super::*;

impl Kubectl {
    pub(super) async fn create_statehub_cluster_crd(
        &self,
    ) -> anyhow::Result<apiextensionsv1::CustomResourceDefinition> {
        let crd = StatehubCluster::crd();
        self.create_crd_impl(crd).await
    }

    pub(super) async fn create_statehub_cluster(
        &self,
        namespace: &str,
    ) -> anyhow::Result<StatehubCluster> {
        let name = self.cluster_name();
        let mut cluster = StatehubCluster::with_namespace(name, namespace);
        let clusters = self.api::<StatehubCluster>().await?;
        if let Ok(existing) = clusters.get(name).await {
            cluster.meta_mut().resource_version = existing.resource_version();
        }

        let pp = self.post_params();

        let cluster = if cluster.resource_version().is_some() {
            clusters.replace(name, &pp, &cluster).await?
        } else {
            clusters.create(&pp, &cluster).await?
        };
        Ok(cluster)
    }

    pub(super) async fn delete_statehub_cluster(&self, name: &str) -> anyhow::Result<()> {
        let clusters = self.api::<StatehubCluster>().await?;
        let dp = self.delete_params();
        clusters.delete(name, &dp).await?;
        Ok(())
    }

    pub(super) async fn delete_statehub_cluster_crd(&self) -> anyhow::Result<()> {
        let crd = StatehubCluster::crd();
        let name = crd.name();
        let crds = self
            .api::<apiextensionsv1::CustomResourceDefinition>()
            .await?;
        let dp = self.delete_params();
        crds.delete(&name, &dp).await?;
        Ok(())
    }

    pub(super) async fn check_statehub_cluster_crd(&self) -> anyhow::Result<Probe> {
        let name = StatehubCluster::crd().name();
        let version = StatehubCluster::api_resource().version;
        let probe = self
            .api::<apiextensionsv1::CustomResourceDefinition>()
            .await?
            .get(&name)
            .await
            .map_or_else(
                |_| Probe::bad("StatehubCluster CRD is not installed"),
                |crd| Probe::good(format!("{}/{}", crd.name(), version)),
            );
        Ok(probe)
    }

    pub(super) async fn check_statehub_cluster_object(&self) -> anyhow::Result<StatehubCluster> {
        let clusters = self.api::<StatehubCluster>().await?;
        let cluster_name = self.cluster_name();
        clusters.get(cluster_name).await.context(format!(
            "StatehubCluster({}) object not found",
            cluster_name
        ))
    }

    pub(super) async fn validate_statehub_cluster_object(
        &self,
        mut clusters: Vec<StatehubCluster>,
    ) -> (Probe, Option<StatehubCluster>) {
        if let Some(cluster) = clusters.pop() {
            if clusters.is_empty() {
                let name = cluster.name();
                let expected_name = self.cluster_name();
                if name == expected_name.0 {
                    (
                        Probe::good(format!("StatehubCluster({name}) found")),
                        Some(cluster),
                    )
                } else {
                    let text = format!(
                        "Found '{name}', expected '{expected_name}`, continue with `{name}`"
                    );
                    (Probe::partial(text), Some(cluster))
                }
            } else {
                (
                    Probe::bad("Multiple StatehubCluster objects found"),
                    Some(cluster),
                )
            }
        } else {
            (Probe::bad("No StatehubCluster objects found"), None)
        }
    }

    async fn create_crd_impl(
        &self,
        mut crd: apiextensionsv1::CustomResourceDefinition,
    ) -> anyhow::Result<apiextensionsv1::CustomResourceDefinition> {
        let name = crd.name();
        let crds = self
            .api::<apiextensionsv1::CustomResourceDefinition>()
            .await?;
        if let Ok(existing) = crds.get(&name).await {
            crd.meta_mut().resource_version = existing.resource_version();
        }

        let pp = self.post_params();

        let crd = if crd.resource_version().is_some() {
            crds.replace(&name, &pp, &crd).await?
        } else {
            crds.create(&pp, &crd).await?
        };
        Ok(crd)
    }

    async fn _delete_crd_impl(&self, name: &str) -> anyhow::Result<()> {
        let crds = self
            .api::<apiextensionsv1::CustomResourceDefinition>()
            .await?;
        let dp = self.delete_params();
        crds.delete(name, &dp).await?;
        Ok(())
    }
}
