//
// Copyright (c) 2022 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

use std::borrow::Cow;
use std::collections::HashMap;

use anyhow::Context;
use k8s_openapi::api::apps::v1 as appsv1;
use k8s_openapi::api::core::v1 as corev1;
use k8s_openapi::apiextensions_apiserver::pkg::apis::apiextensions::v1 as apiextensionsv1;
use kube::api::{self, Api};
use kube::config::{Config, KubeConfigOptions, Kubeconfig};
use kube::{Client, Resource, ResourceExt};
use serde_json as json;
use statehub_cluster_operator::StatehubCluster;

use crate::location::Location;
use crate::v0;

pub(crate) use self::diagnose::Probe;
pub(crate) use self::helm::Helm;

use self::helper::{group_nodes_by_region, group_nodes_by_zone, is_aks, is_eks, NamesAndContexts};
use self::kubeconfig::KubeconfigExt;

mod crd;
mod diagnose;
mod helm;
mod helper;
mod kubeconfig;
mod secret;
mod show;

pub(crate) const KUBE_SYSTEM_NS: &str = "kube-system";
const STATEHUB_CLUSTER_TOKEN_SECRET_TYPE: &str = "statehub.io/cluster-token";
const STATEHUB_CLUSTER_TOKEN_SECRET_NAME: &str = "statehub-cluster-token";

#[derive(Debug)]
pub(crate) struct Kubectl {
    kubeconfig: Kubeconfig,
    name: v0::ClusterName,
    context: String,
}

impl Kubectl {
    pub(crate) fn with_names_and_contexts(
        names: Vec<v0::ClusterName>,
        contexts: Vec<String>,
    ) -> anyhow::Result<Vec<Self>> {
        use NamesAndContexts::*;

        let kubeconfig = Kubeconfig::read()?;

        let kubectl = match (names, contexts).try_into()? {
            GuessEverything => {
                // Use current context for both context and name
                let context = kubeconfig
                    .current_context()
                    .ok_or_else(|| anyhow::anyhow!("No current context in kubeconfig"))?
                    .to_string();
                let name = normalize_name(&context);
                let kubectl = Self {
                    kubeconfig,
                    name,
                    context,
                };
                vec![kubectl]
            }
            ClusterWithCurrentContext(name) => {
                // Use current context for context and name as provided
                let context = kubeconfig
                    .current_context()
                    .ok_or_else(|| anyhow::anyhow!("No current context in kubeconfig"))?
                    .to_string();

                let kubectl = Self {
                    kubeconfig,
                    name,
                    context,
                };
                vec![kubectl]
            }
            NamedContext(context) => {
                anyhow::ensure!(
                    kubeconfig.has_context(&context),
                    "kubeconfig doesn't have context '{}'",
                    context
                );
                let name = normalize_name(&context);
                let kubectl = Self {
                    kubeconfig,
                    name,
                    context,
                };
                vec![kubectl]
            }
            ExactPairs(names_and_contexts) => names_and_contexts
                .into_iter()
                .map(|(name, context)| Self {
                    kubeconfig: kubeconfig.clone(),
                    name,
                    context,
                })
                .collect(),
        };

        Ok(kubectl)
    }

    pub(crate) fn with_context_or_current(context: Option<&str>) -> anyhow::Result<Self> {
        let kubeconfig = Kubeconfig::read()?;
        let current_context = kubeconfig.current_context();
        let context = context
            .or(current_context)
            .ok_or_else(|| anyhow::anyhow!("No current context in kubeconfig"))?
            .to_string();
        let name = normalize_name(&context);

        Ok(Self {
            kubeconfig,
            name,
            context,
        })
    }

    pub(crate) fn default() -> anyhow::Result<Self> {
        let kubeconfig = Kubeconfig::read()?;
        let context = kubeconfig.current_context().unwrap_or_default().to_string();
        let name = normalize_name(&context);

        Ok(Self {
            kubeconfig,
            name,
            context,
        })
    }

    pub(crate) fn context(&self) -> &str {
        &self.context
    }

    pub(crate) fn all_contexts(&self) -> Vec<&str> {
        self.kubeconfig.all_contexts()
    }

    pub(crate) fn cluster_name(&self) -> &v0::ClusterName {
        &self.name
    }

    pub(crate) fn set_cluster_name(self, name: v0::ClusterName) -> Self {
        Self { name, ..self }
    }

    pub(crate) fn set_context(self, context: String) -> Self {
        let name = normalize_name(&context);

        Self {
            name,
            context,
            ..self
        }
    }

    pub(crate) async fn get_operator(
        &self,
    ) -> anyhow::Result<statehub_cluster_operator::cluster::Operator> {
        let config = self.get_config().await?;
        let operator = statehub_cluster_operator::cluster::Operator::with_config(config)?;
        Ok(operator)
    }

    async fn get_config(&self) -> anyhow::Result<Config> {
        let context = Some(self.context.clone());
        let options = KubeConfigOptions {
            context,
            ..KubeConfigOptions::default()
        };
        let kubeconfig = self.kubeconfig.clone();
        Config::from_custom_kubeconfig(kubeconfig, &options)
            .await
            .context("Loading context from kubeconfig")
    }

    async fn get_client(&self) -> anyhow::Result<Client> {
        self.get_config()
            .await?
            .try_into()
            .context("Connecting to Kubernetes cluster")
    }

    async fn all_namespaces(&self) -> anyhow::Result<impl IntoIterator<Item = corev1::Namespace>> {
        let namespaces = self.api().await?;
        let lp = self.list_params();
        Ok(namespaces.list(&lp).await?)
    }

    async fn all_nodes(&self) -> anyhow::Result<impl IntoIterator<Item = corev1::Node>> {
        let nodes = self.api().await?;
        let lp = self.list_params();
        Ok(nodes.list(&lp).await?)
    }

    async fn all_pods(
        &self,
        namespace: &str,
    ) -> anyhow::Result<impl IntoIterator<Item = corev1::Pod>> {
        let pods = self.namespaced_api(namespace).await?;
        let lp = self.list_params();
        Ok(pods.list(&lp).await?)
    }

    fn delete_params(&self) -> api::DeleteParams {
        api::DeleteParams::default()
    }

    fn list_params(&self) -> api::ListParams {
        api::ListParams::default()
    }

    fn post_params(&self) -> api::PostParams {
        api::PostParams::default()
    }

    async fn api<T>(&self) -> anyhow::Result<Api<T>>
    where
        T: Resource,
        <T as Resource>::DynamicType: Default,
    {
        self.get_client().await.map(Api::all)
    }

    async fn namespaced_api<T>(&self, namespace: &str) -> anyhow::Result<Api<T>>
    where
        T: Resource,
        <T as Resource>::DynamicType: Default,
    {
        self.get_client()
            .await
            .map(|client| Api::namespaced(client, namespace))
    }

    pub(crate) async fn get_regions(
        &self,
        zone: bool,
    ) -> anyhow::Result<HashMap<Option<String>, Vec<String>>> {
        let group_nodes = |nodes| {
            if zone {
                group_nodes_by_zone(nodes)
            } else {
                group_nodes_by_region(nodes)
            }
        };

        self.all_nodes().await.map(group_nodes)
    }

    pub(crate) async fn collect_node_locations(&self) -> anyhow::Result<Vec<Location>> {
        self.get_regions(false)
            .await?
            .into_iter()
            .map(|(region, nodes)| {
                region.ok_or_else(|| {
                    anyhow::anyhow!("Cannot determine location for nodes {:?}", nodes)
                })
            })
            .collect::<Result<Vec<_>, _>>()?
            .into_iter()
            .map(|region| region.parse())
            .collect::<Result<Vec<Location>, _>>()
            .map_err(anyhow::Error::msg)
    }

    pub(crate) async fn ensure_statehub_cluster_is_not_installed(&self) -> anyhow::Result<()> {
        if let Ok(cluster) = self.check_statehub_cluster_object().await {
            anyhow::bail!(
                "It looks like this cluster is already registered with Statehub under the name '{}'",
                cluster.name()
            );
        }
        Ok(())
    }

    pub(crate) async fn install_statehub_cluster(
        &self,
        namespace: &str,
    ) -> anyhow::Result<StatehubCluster> {
        let crd = self.create_statehub_cluster_crd().await?;
        log::debug!("Created StatehubCluster CRD: {:#?}", crd);
        let cluster = self.create_statehub_cluster(namespace).await?;
        log::debug!("Created StatehubCluster: {:#?}", cluster);

        let namespace = self
            .get_operator()
            .await?
            .ensure_namespace_exists(&cluster)
            .await?;
        log::debug!("Created namespace: {:#?}", namespace);
        Ok(cluster)
    }

    pub(crate) async fn uninstall_statehub_cluster(&self) -> anyhow::Result<()> {
        let cluster = self.check_statehub_cluster_object().await?;
        let name = cluster.name();
        // let client = self.get_client().await?;
        // let statehub_kubectl = statehub_cluster_operator::cluster::Kubectl::new(client);
        // statehub_kubectl.delete_namespace(&cluster).await?;

        self.delete_statehub_cluster(&name).await?;
        log::debug!("Deleted StatehubCluster");
        Ok(())
    }

    pub(crate) async fn get_statehub_cluster_name_and_namespace(
        &self,
    ) -> anyhow::Result<(String, String)> {
        self.check_statehub_cluster_object()
            .await
            .map(|cluster| (cluster.name(), cluster.spec.namespace))
    }

    pub(crate) async fn cleanup_statehub_cluster(&self) -> anyhow::Result<()> {
        self.delete_statehub_cluster_crd().await?;
        log::debug!("Deleted StatehubCluster CRD");
        Ok(())
    }

    // TODO: implement provider fetch out of cluster
    pub(crate) async fn get_cluster_provider(&self) -> anyhow::Result<v0::Provider> {
        let nodes = self.all_nodes().await?.into_iter().collect::<Vec<_>>();

        if is_aks(&nodes) {
            Ok(v0::Provider::Aks)
        } else if is_eks(&nodes) {
            Ok(v0::Provider::Eks)
        } else {
            Ok(v0::Provider::Generic)
        }
    }

    pub(crate) async fn store_cluster_token(
        &self,
        namespace: &str,
        token: &str,
    ) -> anyhow::Result<corev1::Secret> {
        let token = json::json!({ "cluster-token": token });

        self.set_secret(
            namespace,
            STATEHUB_CLUSTER_TOKEN_SECRET_TYPE,
            STATEHUB_CLUSTER_TOKEN_SECRET_NAME,
            token,
        )
        .await
    }

    pub(crate) async fn list_namespaces(
        &self,
    ) -> anyhow::Result<impl IntoIterator<Item = corev1::Namespace>> {
        self.all_namespaces().await
    }

    pub(crate) async fn list_nodes(
        &self,
    ) -> anyhow::Result<impl IntoIterator<Item = corev1::Node>> {
        self.all_nodes().await
    }

    pub(crate) async fn list_pods(&self) -> anyhow::Result<impl IntoIterator<Item = corev1::Pod>> {
        self.all_pods(KUBE_SYSTEM_NS).await
    }
}

pub(crate) fn extract_cluster_token(secret: &corev1::Secret) -> Option<Cow<'_, str>> {
    secret
        .data
        .as_ref()
        .and_then(|data| data.get("cluster-token"))
        .map(|bytes| String::from_utf8_lossy(&bytes.0))
}

pub(crate) fn get_current_cluster_name() -> Option<v0::ClusterName> {
    Kubeconfig::read().ok()?.current_cluster_name()
}

const CLUSTER_DELIMITER: char = '/';

pub(super) fn normalize_name(name: &str) -> v0::ClusterName {
    if let Some((_, tail)) = name.rsplit_once(CLUSTER_DELIMITER) {
        tail.into()
    } else {
        name.into()
    }
}
