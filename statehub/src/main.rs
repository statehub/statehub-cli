//
// Copyright (c) 2022 RepliXio Ltd. All rights reserved.
// Use is subject to license terms.
//

#![cfg_attr(feature = "pedantic", warn(clippy::pedantic))]
#![warn(clippy::use_self)]
#![warn(clippy::map_flatten)]
#![warn(clippy::map_unwrap_or)]
#![warn(deprecated_in_future)]
#![warn(future_incompatible)]
#![warn(noop_method_call)]
#![warn(unreachable_pub)]
#![warn(missing_debug_implementations)]
#![warn(rust_2018_compatibility)]
#![warn(rust_2021_compatibility)]
#![warn(rust_2018_idioms)]
#![warn(unused)]
#![deny(warnings)]
#![cfg_attr(result_option_inspect, feature(result_option_inspect))]

use serde_json as json;
use statehub_api::v0;
use statehub_location as location;

use output::Output;
use statehub::Cli;

// statehub API implementation
mod api;
// K8s interface and helpers
mod k8s;
// `Output` wrapper
mod output;
// `Show` trait definition and impls
mod show;
// Main tool bussines logic
mod statehub;
// custom traits
mod traits;

#[tokio::main]
async fn main() {
    dotenv::dotenv().ok();
    pretty_env_logger::init_custom_env("STATEHUB_LOG");
    Cli::execute().await.report()
}

struct CliResult {
    json: bool,
    result: anyhow::Result<()>,
}

impl CliResult {
    fn new(json: bool, result: anyhow::Result<()>) -> Self {
        Self { json, result }
    }

    fn report(self) {
        match self.result {
            Ok(()) => std::process::exit(0),
            Err(err) if self.json => {
                if let Some(err) = err.downcast_ref::<v0::Error>() {
                    println!("{}", json::to_string(err).unwrap_or_default());
                } else {
                    eprintln!("Error: {:?}", err);
                }
                std::process::exit(1)
            }
            Err(err) => {
                eprintln!("Error: {:?}", err);
                std::process::exit(1)
            }
        }
    }
}
